/**
* Author : phu.nguyenluu@gmail.com
* Setup : 10/08/2021
*/

import { RolesEnum } from '@decorators/roles.decorator';

export interface JwtDecodeResponse {
  id: string,
  email: string,
  role: RolesEnum,
  iat: number,
  exp: number,
}
