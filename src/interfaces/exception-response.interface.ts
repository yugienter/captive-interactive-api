/**
* Author : phu.nguyenluu@gmail.com
* Setup : 10/08/2021
*/

export interface ExceptionResponse {
  readonly message: unknown;

  readonly statusCode: number;

  readonly error: string;
}
