/**
* Author : phu.nguyenluu@gmail.com
* Setup : 10/08/2021
*/

import { SetMetadata } from '@nestjs/common';

export enum RolesEnum {
  admin = 'admin',
  brand = 'brand',
  host = 'host',
  user = 'user',
}

export const Roles = (...roles: RolesEnum[]) => SetMetadata('roles', roles);
