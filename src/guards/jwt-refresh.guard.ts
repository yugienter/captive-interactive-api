/**
* Author : phu.nguyenluu@gmail.com
* Setup : 10/08/2021
*/

import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export default class JwtRefreshGuard extends AuthGuard('refreshToken') { }
