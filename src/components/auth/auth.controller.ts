/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 10/08/2021
 */

import {
  Body,
  Controller,
  HttpCode,
  Get,
  Post,
  Param,
  Request,
  UseGuards,
  NotFoundException,
  ForbiddenException,
  HttpStatus,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBody,
  ApiOkResponse,
  ApiInternalServerErrorResponse,
  ApiUnauthorizedResponse,
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiBadRequestResponse,
  ApiConflictResponse,
  ApiNoContentResponse,
  ApiExtraModels,
  getSchemaPath,
} from '@nestjs/swagger';
import { JwtService } from '@nestjs/jwt';
import { Request as ExpressRequest } from 'express';
import { MailerService } from '@nestjs-modules/mailer';

import UsersService from '@components/users/users.service';
import JwtAccessGuard from '@guards/jwt-access.guard';
import RolesGuard from '@guards/roles.guard';
import { UserEntity } from '@components/users/schemas/users.schema';
import WrapResponseInterceptor from '@interceptors/wrap-response.interceptor';
import AuthBearer from '@decorators/auth-bearer.decorator';
import { Roles, RolesEnum } from '@decorators/roles.decorator';
import authConstants from '@components/auth/auth-constants';
import CreateNewUserDto from '@components/users/dto/create-new-user.dto';
import { DecodedUser } from './interfaces/decoded-user.interface';
import LocalAuthGuard from './guards/local-auth.guard';
import AuthService from './auth.service';
import SignInDto from './dto/sign-in.dto';
import SignUpDto from './dto/sign-up.dto';
// import VerifyUserDto from './dto/verify-user.dto';
import JwtTokensDto from './dto/jwt-tokens.dto';
import CreatePasswordDto from './dto/create-password.dto';

@ApiTags('Auth')
@UseInterceptors(WrapResponseInterceptor)
@ApiExtraModels(JwtTokensDto)
@Controller('auth')
export default class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly jwtService: JwtService,
    private readonly usersService: UsersService,
    private readonly mailerService: MailerService,
  ) { }

  @ApiBody({ type: SignInDto })
  @ApiOkResponse({
    schema: {
      type: 'object',
      properties: { data: { $ref: getSchemaPath(JwtTokensDto) } },
    },
    description: 'Returns jwt tokens',
  })
  @ApiBadRequestResponse({
    schema: {
      type: 'object',
      example: {
        message: [{
          target: {
            email: 'string',
            password: 'string',
          },
          value: 'string',
          property: 'string',
          children: [],
          constraints: {},
        }],
        error: 'Bad Request',
      },
    },
    description: '400. ValidationException',
  })
  @ApiInternalServerErrorResponse({
    schema: { type: 'object', example: { message: 'string', details: {} } },
    description: '500. InternalServerError',
  })
  @ApiBearerAuth()
  @HttpCode(HttpStatus.OK)
  @UseGuards(LocalAuthGuard)
  @Post('sign-in')
  async signIn(@Request() req: ExpressRequest): Promise<JwtTokensDto> {
    const user = req.user as UserEntity;

    return this.authService.login(user);
  }

  // @ApiBody({ type: SignUpDto })
  // @ApiOkResponse({ description: '201, Success', })
  // @ApiBadRequestResponse({
  //   schema: {
  //     type: 'object',
  //     example: {
  //       message: [{
  //         target: {
  //           email: 'string',
  //           password: 'string',
  //         },
  //         value: 'string',
  //         property: 'string',
  //         children: [],
  //         constraints: {},
  //       },],
  //       error: 'Bad Request',
  //     },
  //   },
  //   description: '400. ValidationException',
  // })
  // @ApiConflictResponse({
  //   schema: { type: 'object', example: { message: 'string', }, },
  //   description: '409. ConflictResponse',
  // })
  // @ApiInternalServerErrorResponse({
  //   schema: { type: 'object', example: { message: 'string', details: {}, }, },
  //   description: '500. InternalServerError',
  // })
  // @HttpCode(HttpStatus.CREATED)
  // @Post('sign-up')
  // async signUp(@Body() user: SignUpDto): Promise<any> {
  //   const { _id, email }: UserEntity = await this.usersService.create(user);
  //   const token = this.authService.createVerifyToken(_id);

  //   try {
  //     await this.mailerService.sendMail({
  //       to: email,
  //       from: process.env.MAILER_FROM_EMAIL,
  //       subject: authConstants.mailer.verifyEmail.subject,
  //       template: authConstants.mailer.verifyEmail.template,
  //       context: { token, email, host: process.env.SERVER_HOST, },
  //     });
  //   } catch (error) {
  //     throw new ForbiddenException(error);
  //   }

  //   return { message: 'Success! please verify your email' };
  // }

  @ApiBody({ type: SignUpDto })
  @ApiOkResponse({ description: '201, Success' })
  @ApiBadRequestResponse({
    schema: {
      type: 'object',
      example: {
        message: [{
          target: {
            email: 'string',
            password: 'string',
          },
          value: 'string',
          property: 'string',
          children: [],
          constraints: {},
        }],
        error: 'Bad Request',
      },
    },
    description: '400. ValidationException',
  })
  @ApiConflictResponse({
    schema: { type: 'object', example: { message: 'string' } },
    description: '409. ConflictResponse',
  })
  @ApiInternalServerErrorResponse({
    schema: { type: 'object', example: { message: 'string', details: {} } },
    description: '500. InternalServerError',
  })
  @HttpCode(HttpStatus.CREATED)
  @Post('create-password')
  async createPassword(@Body() user: CreatePasswordDto): Promise<any> {
    return this.usersService.createPassword(user);
  }

  @ApiBody({ type: CreateNewUserDto })
  @ApiOkResponse({ description: '201, Success' })
  @ApiBadRequestResponse({
    schema: {
      type: 'object',
      example: {
        message: [{
          target: {
            email: 'string',
          },
          value: 'string',
          property: 'string',
          children: [],
          constraints: {},
        }],
        error: 'Bad Request',
      },
    },
    description: '400. ValidationException',
  })
  @ApiConflictResponse({
    schema: { type: 'object', example: { message: 'string' } },
    description: '409. ConflictResponse',
  })
  @ApiInternalServerErrorResponse({
    schema: { type: 'object', example: { message: 'string', details: {} } },
    description: '500. InternalServerError',
  })
  @ApiBearerAuth()
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(RolesGuard)
  @Roles(RolesEnum.admin)
  @Post('create-new-user')
  async createNewUser(@Body() newUser: CreateNewUserDto): Promise<any> {
    const { _id, email, role } = await this.usersService.createNewUser(newUser);
    const token = this.authService.createVerifyToken(_id);

    try {
      await this.mailerService.sendMail({
        to: email,
        from: process.env.MAILER_FROM_EMAIL,
        subject: authConstants.mailer.createNewPassword.subject,
        template: authConstants.mailer.createNewPassword.template,
        context: {
          email, token, role, host: process.env.CLIENT_APP,
        },
      });
    } catch (error) {
      throw new ForbiddenException(error);
    }

    return {
      message: 'Success! please create new password from link in your email',
    };
  }

  @ApiNoContentResponse({ description: 'No content. 204' })
  @ApiNotFoundResponse({
    schema: {
      type: 'object',
      example: { message: 'string', error: 'Not Found' },
    },
    description: 'User was not found',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Get('verify/:token')
  async verifyUser(@Param('token') token: string): Promise<{} | never> {
    const { id } = await this.authService.verifyEmailVerToken(
      token,
      authConstants.jwt.secrets.accessToken,
    );
    const foundUser = await this.usersService.getById(id, false);

    if (!foundUser) {
      throw new NotFoundException('The user does not exist or verified');
    }

    return this.usersService.update(foundUser._id, { verified: true });
  }

  @ApiOkResponse({
    type: UserEntity,
    description: '200, returns a decoded user from access token',
  })
  @ApiUnauthorizedResponse({
    schema: { type: 'object', example: { message: 'string' } },
    description: '403, says you Unauthorized',
  })
  @ApiInternalServerErrorResponse({
    schema: { type: 'object', example: { message: 'string', details: {} } },
    description: '500. InternalServerError',
  })
  @ApiBearerAuth()
  @UseGuards(JwtAccessGuard)
  @Get('token')
  async getUserByAccessToken(
    @AuthBearer() token: string,
  ): Promise<DecodedUser | never> {
    const decodedUser: DecodedUser | null = await this.authService.verifyToken(
      token,
      authConstants.jwt.secrets.accessToken,
    );

    if (!decodedUser) {
      throw new ForbiddenException('Incorrect token');
    }

    // const { exp, iat, ...user } = decodedUser;
    const { ...user } = decodedUser;

    return user;
  }
}
