/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 10/08/2021
 */

import { ApiProperty } from '@nestjs/swagger';

export default class VerifyUserDto {
  @ApiProperty({ type: String })
  readonly email: string = '';
}
