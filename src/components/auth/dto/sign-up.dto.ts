/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 10/08/2021
 */

import {
  IsNotEmpty,
  MinLength,
  MaxLength,
  IsString,
  IsEmail,
  Matches,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { RolesEnum } from '@decorators/roles.decorator';

export default class SignUpDto {
  @ApiProperty({ type: String })
  @IsNotEmpty()
  @IsString()
  @IsEmail()
  @MinLength(3)
  @MaxLength(128)
  readonly email: string = '';

  @ApiProperty({ type: String })
  @IsNotEmpty()
  @IsString()
  @MinLength(8)
  @MaxLength(64)
  @Matches(/^(?=.*[0-9])(?=.*[a-zA-Z])/,
    { message: 'at least 1 number or 1 symbol' })
  readonly password: string = '';

  @ApiProperty({ enum: RolesEnum, default: RolesEnum.brand })
  @IsNotEmpty()
  @IsString()
  readonly role: RolesEnum = RolesEnum.user;
}
