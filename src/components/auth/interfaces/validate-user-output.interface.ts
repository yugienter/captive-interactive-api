/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 10/08/2021
 */

import { Types } from 'mongoose';
import { RolesEnum } from '@decorators/roles.decorator';

export interface ValidateUserOutput {
  id: Types.ObjectId;
  email?: string;
  role?: RolesEnum;
}
