/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 10/08/2021
 */

import { Types } from 'mongoose';

export interface LoginPayload {
  readonly id?: Types.ObjectId;

  readonly email: string;

  readonly role?: string;
}
