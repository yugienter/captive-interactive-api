/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 10/08/2021
 */

import { Types } from 'mongoose';

export interface DecodedUser {
  readonly id: Types.ObjectId;

  readonly email: string;

  readonly password: string;

  readonly role: string;

  readonly iat?: number;

  readonly exp?: number;
}
