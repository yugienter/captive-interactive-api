/**
* Author : phu.nguyenluu@gmail.com
* Setup : 12/08/2021
*/

import { ObjectID } from 'mongodb';
import { EnforceDocument, Model } from 'mongoose';
import { BaseEntity } from './base.schema';

export class BaseRepository<T extends BaseEntity> {
  constructor(
    private readonly _model: Model<T>,
  ) { }

  async findAll(filter = {}): Promise<T[]> {
    return this._model.find(filter).exec();
  }

  async findOne(filter = {}): Promise<T | null> {
    return this._model.findOne(filter).exec();
  }

  async findById(id: ObjectID): Promise<T | null> {
    return this._model.findById(id).exec();
  }

  async create(item: any): Promise<T> {
    return this._model.create(item);
  }

  async delete(id: ObjectID): Promise<T | null> {
    return this._model.findByIdAndRemove(id).exec();
  }

  public updateById(id: string, data: {}): Promise<EnforceDocument<T, {}> | null> {
    return this._model.findByIdAndUpdate(id, data, { new: true }).exec();
  }

  async clearCollection(filter = {}): Promise<{ ok?: number; n?: number; }> {
    return this._model.deleteMany(filter).exec();
  }
}
