/**
* Author : phu.nguyenluu@gmail.com
* Setup : 25/08/2021
*/

const currentPage = 0;
const perPage = 10;

export const constants = {
  currentPage,
  perPage,
};
