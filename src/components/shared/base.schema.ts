/**
* Author : phu.nguyenluu@gmail.com
* Setup : 12/08/2021
*/

import { ApiProperty } from '@nestjs/swagger';
import { ObjectId } from 'mongodb';
import { Document, Types } from 'mongoose';

export class BaseEntity extends Document {
  @ApiProperty({ type: String })
  readonly _id: Types.ObjectId = new ObjectId();
}
