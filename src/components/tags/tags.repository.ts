/**
* Author : phu.nguyenluu@gmail.com
* Setup : 15/08/2021
*/

import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { BaseRepository } from '@shared/base.repository';
import { Model } from 'mongoose';
import { TagEntity } from './schemas/tags.schema';
import tagsConstants from './tags-constants';

@Injectable()
export default class TagsRepository extends BaseRepository<TagEntity> {
  constructor(
    @InjectModel(tagsConstants.models.tags) private tagsModel: Model<TagEntity>,
  ) {
    super(tagsModel);
  }

  async findAllWithParentName(): Promise<TagEntity[]> {
    return this.tagsModel.find()
      .populate({ path: tagsConstants.models.tags, select: 'name' });
  }
}
