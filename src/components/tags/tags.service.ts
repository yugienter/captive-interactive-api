/**
* Author : phu.nguyenluu@gmail.com
* Setup : 15/08/2021
*/

import { Injectable } from '@nestjs/common';
import { Types } from 'mongoose';
import CreateTagDto from './dto/create-tag.dto';
import { TagEntity } from './schemas/tags.schema';
import TagsRepository from './tags.repository';

@Injectable()
export class TagsService {
  constructor(
    private readonly tagsRepository: TagsRepository,
  ) { }

  public async create(tag: CreateTagDto): Promise<TagEntity> {
    const { name, parentId } = tag;
    const newTag: any = {};
    if (parentId) {
      newTag.parentId = Types.ObjectId(parentId);
      const parentTag: TagEntity | null = await this.tagsRepository.findById(newTag.parentId);
      newTag.parentName = parentTag?.name;
    }
    newTag.name = name;
    await this.tagsRepository.create(newTag);
    return newTag;
  }

  public async getAll(): Promise<TagEntity[]> {
    return this.tagsRepository.findAllWithParentName();
  }
}
