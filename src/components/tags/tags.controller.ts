/**
* Author : phu.nguyenluu@gmail.com
* Setup : 15/08/2021
*/

import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  UseInterceptors,
  Post,
  Get,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiConflictResponse,
  ApiExtraModels,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
  getSchemaPath,
} from '@nestjs/swagger';
import WrapResponseInterceptor from '@interceptors/wrap-response.interceptor';
import CreateTagDto from './dto/create-tag.dto';
import { TagEntity } from './schemas/tags.schema';
import { TagsService } from './tags.service';

@ApiTags('Tags')
@UseInterceptors(WrapResponseInterceptor)
@ApiExtraModels(TagEntity)
@Controller('tags')
export class TagsController {
  constructor(private readonly tagsService: TagsService) { }

  @ApiBody({ type: CreateTagDto })
  @ApiOkResponse({
    schema: {
      type: 'object',
      properties: { data: { $ref: getSchemaPath(CreateTagDto) } },
    },
    description: 'Return Created TAG',
  })
  @ApiBadRequestResponse({
    schema: {
      type: 'object',
      example: {
        message: [{
          target: {
            name: 'string',
            parentId: 'string',
          },
          value: 'string',
          property: 'string',
          children: [],
          constraints: {},
        }],
        error: 'Bad Request',
      },
    },
    description: '400. ValidationException',
  })
  @ApiConflictResponse({
    schema: { type: 'object', example: { message: 'string' } },
    description: '409. ConflictResponse',
  })
  @ApiInternalServerErrorResponse({
    schema: { type: 'object', example: { message: 'string', details: {} } },
    description: '500. InternalServerError',
  })
  @HttpCode(HttpStatus.OK)
  @Post()
  async create(@Body() tag: CreateTagDto): Promise<TagEntity> {
    return this.tagsService.create(tag);
  }

  @ApiOkResponse({
    description: '200. Success. Returns all users',
    schema: {
      type: 'object',
      properties: { data: { $ref: getSchemaPath(TagEntity) } },
    },
  })
  @ApiUnauthorizedResponse({
    schema: { type: 'object', example: { message: 'string' } },
    description: '401. UnauthorizedException.',
  })
  @Get()
  async getAllVerifiedUsers(): Promise<TagEntity[] | []> {
    return this.tagsService.getAll();
  }
}
