/**
* Author : phu.nguyenluu@gmail.com
* Setup : 15/08/2021
*/

import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { ObjectId } from 'mongodb';
import { Schema, Document, Types } from 'mongoose';

import tagsConstants from '../tags-constants';

export class TagEntity extends Document {
  @ApiProperty({ type: String })
  readonly _id: Types.ObjectId = new ObjectId();

  @ApiProperty({ type: String })
  readonly name: string = '';

  @ApiPropertyOptional({ type: Types.ObjectId })
  readonly parentId?: Types.ObjectId;

  @ApiPropertyOptional({ type: String })
  readonly parentName?: string;
}

export const TagSchema = new Schema(
  {
    name: { type: String, required: true },
    parentId: { type: Types.ObjectId, ref: tagsConstants.models.tags },
  },
  {
    versionKey: false,
    collection: tagsConstants.models.tags,
    timestamps: true,
    toObject: {
      virtuals: true,
      versionKey: false,
      transform(doc, ret) {
        delete ret._id;
      },
    },
    toJSON: {
      virtuals: true,
      versionKey: false,
      transform(doc, ret) {
        delete ret._id;
      },
    },
  },
);
TagSchema.index({ name: 1, parentId: 1 }, { unique: true });

TagSchema.virtual(tagsConstants.models.tags, {
  ref: tagsConstants.models.tags,
  localField: 'parentId',
  foreignField: '_id',
  justOne: true,
});
