/**
* Author : phu.nguyenluu@gmail.com
* Setup : 15/08/2021
*/

import { PartialType } from '@nestjs/swagger';
import CreateTagDto from './create-tag.dto';

export default class UpdateTagDto extends PartialType(CreateTagDto) { }
