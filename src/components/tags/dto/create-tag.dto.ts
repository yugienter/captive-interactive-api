/**
* Author : phu.nguyenluu@gmail.com
* Setup : 15/08/2021
*/

import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export default class CreateTagDto {
  @ApiProperty({ type: String })
  @IsNotEmpty()
  @IsString()
  readonly name: string = '';

  @ApiPropertyOptional({ type: String })
  @IsOptional()
  @IsString()
  readonly parentId?: string = '';
}
