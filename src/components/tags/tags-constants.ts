/**
* Author : phu.nguyenluu@gmail.com
* Setup : 15/08/2021
*/

const tagsConstants = {
  models: {
    tags: 'tags',
  },
  ref: {
    tags: 'Tags',
  },
};

export default tagsConstants;
