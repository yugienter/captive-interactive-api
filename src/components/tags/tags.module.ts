/**
* Author : phu.nguyenluu@gmail.com
* Setup : 15/08/2021
*/

import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TagsService } from './tags.service';
import { TagsController } from './tags.controller';
import tagsConstants from './tags-constants';
import { TagSchema } from './schemas/tags.schema';
import TagsRepository from './tags.repository';

@Module({
  imports: [
    MongooseModule.forFeature([{
      name: tagsConstants.models.tags,
      schema: TagSchema,
    }]),
  ],
  controllers: [TagsController],
  providers: [TagsService, TagsRepository],
  exports: [TagsService, TagsRepository],
})
export class TagsModule { }
