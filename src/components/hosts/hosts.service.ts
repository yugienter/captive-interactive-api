/**
 * Author : phu.nguyenluu@gmail.com
 * Update : 25/08/2021
 */

import tagsConstants from '@components/tags/tags-constants';
import TagsRepository from '@components/tags/tags.repository';
import UsersRepository from '@components/users/users.repository';
import { RolesEnum } from '@decorators/roles.decorator';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Request } from 'express';
import { constants } from '@shared/constant';
import HostDto from './dto/host.dto';
import hostsConstants from './hosts-constants';
import HostsRepository from './hosts.repository';
import { HostEntity } from './schemas/hosts.schema';
import HostResDto from './dto/host-response.dto';
import HostRateRangeResponseDto from './dto/host-rate-range-response.dto';

const { currentPage, perPage } = constants;

@Injectable()
export class HostsService {
  constructor(
    private readonly _userRepository: UsersRepository,
    private readonly _tagRepository: TagsRepository,
    private readonly _hostRepository: HostsRepository,
    @InjectModel(hostsConstants.models.hosts)
    private readonly _hostModel: Model<HostEntity>,
  ) { }

  public async import(hosts: HostDto[]): Promise<{ message: string }> {
    const allTags = await this._tagRepository.findAll();
    const imports: any[] = [];

    for await (const host of hosts) {
      const importingHost = new this._hostModel(host);

      if (importingHost.email) {
        const user = { email: importingHost.email, role: RolesEnum.host };
        const currentUser = await this._userRepository.findOne(user);

        if (!currentUser) await this._userRepository.create(user);
      }

      // map tags' name to corresponding _id
      importingHost.tags = allTags
        .filter((tagInfo) => host.tags?.includes(tagInfo.name))
        .map((tagInfo) => tagInfo._id);

      // import by an insertion operation of whole document
      // TODO: if using replaceOne instead, specify unique fields to filter
      imports.push({ insertOne: { document: importingHost.toJSON() } });
    }

    try {
      await this._hostRepository.bulkWrite(imports);
      return { message: 'Success' };
    } catch (error) {
      throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public async getRateRange(): Promise<HostRateRangeResponseDto> {
    const hostWithMaxRate: HostEntity | null = await this._hostModel
      .findOne()
      .sort({ rates: -1 })
      .select('rates')
      .exec();
    const hostWithMinRate: HostEntity | null = await this._hostModel
      .findOne()
      .sort({ rates: 1 })
      .select('rates')
      .exec();
    const max = hostWithMaxRate?.rates || 0;
    const min = hostWithMinRate?.rates || 0;
    return { min, max };
  }

  /**
   * Author : phu.nguyenluu@gmail.com
   * Updated : 25/08/2021
   */
  public async getAllHost(req: Request): Promise<HostResDto> {
    const options = this.generateHostQuery(req);

    const aggregate: any = [
      {
        $set: {
          followers: {
            $add: [
              { $ifNull: ['$social.facebook.followers', 0] },
              { $ifNull: ['$social.instagram.followers', 0] },
              { $ifNull: ['$social.tiktok.followers', 0] },
            ],
          },
        },
      },
    ];

    if (Object.keys(options).length) aggregate.push({ $match: options });

    const { sort_col: sortCol, sort_type: sortType } = req.query;
    let sort: any = { createdAt: -1 };
    if (
      sortCol
      && typeof sortCol === 'string'
      && sortType
      && [1, -1].includes(parseInt(sortType as any, 10))
    ) {
      sort = { [sortCol]: parseInt(sortType as any, 10) };
    }
    aggregate.push({ $sort: sort });

    const page: number = parseInt(req.query.page as any, 10) || currentPage;
    const limit: number = parseInt(req.query.per_page as any, 10) || perPage;
    aggregate.push({ $skip: page * limit });
    aggregate.push({ $limit: limit });
    aggregate.push({
      $lookup: {
        from: tagsConstants.models.tags,
        localField: 'tags',
        foreignField: '_id',
        as: 'tags',
      },
    });

    try {
      const query = await this._hostRepository.aggregate(aggregate);
      const total = await this._hostRepository.count(options);
      return {
        data: query,
        total,
        page,
        perPage: limit,
        lastPage: Math.ceil(total / limit),
      };
    } catch (error) {
      console.log(error);
      throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  private customRangeFilter = (start: any, end: any): any => {
    const row: any = {};
    if (start) row.$gte = parseInt(start, 10);
    if (end) row.$lte = parseInt(end, 10);
    return row;
  };

  private generateHostQuery = (req: Request): any => {
    const options: any = {};
    const and: any[] = [];
    const or = [];

    const {
      search,
      exp_start: expStart,
      exp_end: expEnd,
      age_start: ageStart,
      age_end: ageEnd,
      rate_start: rateStart,
      rate_end: rateEnd,
      gender,
      tag,
    } = req.query;
    if (search) {
      or.push(
        { name: new RegExp(search.toString(), 'i') },
        { aboutMe: new RegExp(search.toString(), 'i') },
      );
    }

    // filter experience
    const experience: any = this.customRangeFilter(expStart, expEnd);
    if (Object.keys(experience).length) and.push({ experience });

    // filter age
    const age: any = this.customRangeFilter(ageStart, ageEnd);
    if (Object.keys(age).length) and.push({ age });

    // filter rates
    const rates: any = this.customRangeFilter(rateStart, rateEnd);
    if (Object.keys(rates).length) and.push({ rates });

    // filter gender
    if (gender) and.push({ sex: gender });

    if (tag) {
      const tags = Array.isArray(tag) ? tag : [tag];
      const tagsId: any = tags.map((tagItem) => Types.ObjectId(String(tagItem)));
      and.push({
        tags: { $in: tagsId },
      });
    }

    if (or.length > 0) options.$or = or;
    if (and.length > 0) options.$and = and;

    return options;
  };
}
