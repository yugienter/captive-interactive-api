/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 12/08/2021
 */

import { Roles, RolesEnum } from '@decorators/roles.decorator';
import RolesGuard from '@guards/roles.guard';
import WrapResponseInterceptor from '@interceptors/wrap-response.interceptor';
import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiBody,
  ApiConflictResponse,
  ApiExtraModels,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiQuery,
  ApiTags,
  ApiUnauthorizedResponse,
  getSchemaPath,
} from '@nestjs/swagger';
import { Request } from 'express';
import HostDto from './dto/host.dto';
import HostListDto from './dto/host-list.dto';
import { HostsService } from './hosts.service';
import { HostEntity } from './schemas/hosts.schema';
import HostResDto from './dto/host-response.dto';
import HostRateRangeResponseDto from './dto/host-rate-range-response.dto';

@ApiTags('Host')
@UseInterceptors(WrapResponseInterceptor)
@ApiExtraModels(HostDto)
@ApiBearerAuth()
@Controller('hosts')
export class HostsController {
  constructor(private readonly hostService: HostsService) { }

  @ApiBody({ type: HostListDto })
  @ApiOkResponse({
    schema: { type: 'object', example: { message: 'Success' } },
    description: 'Return Created TAG',
  })
  @ApiBadRequestResponse({
    schema: {
      type: 'object',
      example: {
        message: [{
          target: { name: 'string', parentId: 'string' },
          value: 'string',
          property: 'string',
          children: [],
          constraints: {},
        }],
        error: 'Bad Request',
      },
    },
    description: '400. ValidationException',
  })
  @ApiConflictResponse({
    schema: { type: 'object', example: { message: 'string' } },
    description: '409. ConflictResponse',
  })
  @ApiInternalServerErrorResponse({
    schema: { type: 'object', example: { message: 'string', details: {} } },
    description: '500. InternalServerError',
  })
  @HttpCode(HttpStatus.OK)
  @UseGuards(RolesGuard)
  @Roles(RolesEnum.admin)
  @Post('import')
  async import(@Body() data: HostListDto): Promise<{ message: string }> {
    return this.hostService.import(data.hosts);
  }

  @ApiOkResponse({
    description: '200. Success. Returns all users',
    schema: {
      type: 'object',
      properties: { data: { $ref: getSchemaPath(HostEntity) } },
    },
  })
  @ApiUnauthorizedResponse({
    schema: { type: 'object', example: { message: 'string' } },
    description: '401. UnauthorizedException.',
  })
  /**
   * Author : phu.nguyenluu@gmail.com
   * Updated : 25/08/2021
   */
  @ApiQuery({ name: 'search', required: false })
  @ApiQuery({ name: 'exp_start', required: false })
  @ApiQuery({ name: 'exp_end', required: false })
  @ApiQuery({ name: 'age_start', required: false })
  @ApiQuery({ name: 'age_end', required: false })
  @ApiQuery({ name: 'rate_start', required: false })
  @ApiQuery({ name: 'rate_end', required: false })
  @ApiQuery({ name: 'gender', enum: ['F', 'M'], required: false })
  @ApiQuery({
    name: 'tag',
    enum: [
      '61195e7415d7fea7a0233ed3',
      '611960f8fc42fda9b180ad04',
      '61196140fc42fda9b180ad15',
    ],
    required: false,
    isArray: true,
    description:
      'Example API of choosing categories,'
      + 'in turn correspond [Raw Fod, Gaming, Baby Wear]',
  })
  @Get()
  @UseGuards(RolesGuard)
  @Roles(RolesEnum.brand, RolesEnum.admin)
  async getAllHost(@Req() req: Request): Promise<HostResDto> {
    return this.hostService.getAllHost(req);
  }

  /**
   * Author : phu.nguyenluu@gmail.com
   * Updated : 28/08/2021
   */
  @ApiOkResponse({
    description: '200. Success. Returns max and min rate range of host',
    schema: {
      type: 'object',
      properties: { data: { $ref: getSchemaPath(HostRateRangeResponseDto) } },
    },
  })
  @ApiNotFoundResponse({
    description: '404. NotFoundException. Rate of host was not found',
  })
  @Get('rates')
  async getRateRange(): Promise<HostRateRangeResponseDto> {
    return this.hostService.getRateRange();
  }
}
