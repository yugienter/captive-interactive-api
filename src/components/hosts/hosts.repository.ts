import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { BaseRepository } from '@shared/base.repository';
import { Model } from 'mongoose';
import hostsConstants from './hosts-constants';
import { HostEntity } from './schemas/hosts.schema';

@Injectable()
export default class HostsRepository extends BaseRepository<HostEntity> {
  constructor(
    @InjectModel(hostsConstants.models.hosts)
    private hostModel: Model<HostEntity>,
  ) {
    super(hostModel);
  }

  /**
   * Author : phu.nguyenluu@gmail.com
   * Updated : 25/08/2021
   */
  public async aggregate(options: any): Promise<any> {
    return this.hostModel.aggregate(options);
  }

  public count(options: {}) {
    return this.hostModel.count(options).exec();
  }

  public bulkWrite(ops: any[]) {
    return this.hostModel.bulkWrite(ops);
  }
}
