/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 12/08/2021
 */

import UsersRepository from '@components/users/users.repository';
import { Test, TestingModule } from '@nestjs/testing';
import { HostsService } from './hosts.service';

describe('HostsService', () => {
  let service: HostsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: HostsService,
          useValue: {},
        },
        {
          provide: UsersRepository,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<HostsService>(HostsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
