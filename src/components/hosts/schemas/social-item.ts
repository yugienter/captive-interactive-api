/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 12/08/2021
 */

import { ApiPropertyOptional } from '@nestjs/swagger';

export class SocialItem {
  @ApiPropertyOptional({ type: String })
  link?: string;

  @ApiPropertyOptional({ type: String })
  followers?: number;
}
