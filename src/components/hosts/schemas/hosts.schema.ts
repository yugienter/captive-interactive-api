/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 12/08/2021
 */

import tagsConstants from '@components/tags/tags-constants';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { ObjectId } from 'mongodb';
import { Document, Schema, Types } from 'mongoose';
import hostsConstants from '../hosts-constants';
import { SocialList } from './social-list';

export class HostEntity extends Document {
  @ApiProperty({ type: String })
  readonly _id: Types.ObjectId = new ObjectId();

  @ApiPropertyOptional({ type: String })
  readonly email?: string;

  @ApiPropertyOptional({ type: String })
  readonly name?: string;

  @ApiPropertyOptional({ type: String })
  readonly title?: string;

  @ApiPropertyOptional({ type: Number })
  readonly age?: number;

  @ApiPropertyOptional({ type: String })
  readonly sex?: string;

  @ApiPropertyOptional({ type: Number })
  readonly weight?: number;

  @ApiPropertyOptional({ type: Number })
  readonly height?: number;

  @ApiPropertyOptional({ type: SocialList })
  readonly social?: SocialList;

  @ApiPropertyOptional({ type: String })
  readonly phoneNumber?: string;

  @ApiPropertyOptional({ type: String })
  readonly photo?: string[];

  @ApiPropertyOptional({ type: String })
  readonly rates?: number;

  @ApiPropertyOptional({ type: String })
  readonly experience?: number;

  @ApiPropertyOptional({ type: String })
  readonly aboutMe?: string;

  @ApiPropertyOptional({ type: String })
  readonly porfolio?: string;

  @ApiPropertyOptional({ type: String })
  tags?: Types.ObjectId[];
}

const SocialSchema = new Schema(
  {
    facebook: { type: Object },
    instagram: { type: Object },
    tiktok: { type: Object },
  },
  {
    versionKey: false,
    collection: 'social',
    timestamps: true,
    toObject: {
      virtuals: true,
      versionKey: false,
      transform(doc, ret) {
        delete ret._id;
      },
    },
    toJSON: {
      virtuals: true,
      versionKey: false,
      transform(doc, ret) {
        delete ret._id;
      },
    },
  },
);

export const HostSchema = new Schema(
  {
    // userId: { type: Types.ObjectId, required: true, unique: true },
    email: { type: String, required: false },
    name: { type: String, required: false },
    title: { type: String, required: false },
    age: { type: Number, required: false },
    sex: { type: String, required: false },
    weight: { type: Number, required: false },
    height: { type: Number, required: false },
    social: { type: SocialSchema, default: {} },
    phoneNumber: { type: String, required: false },
    photo: { type: Array, default: [] },
    rates: { type: Number, required: false, default: 0 },
    experience: { type: Number, required: false, default: 0 },
    aboutMe: { type: String, required: false },
    porfolio: { type: String, required: false },
    tags: [
      {
        type: Types.ObjectId,
        ref: tagsConstants.models.tags,
      },
    ],
  },
  {
    versionKey: false,
    collection: hostsConstants.models.hosts,
    timestamps: true,
    toObject: {
      virtuals: true,
      versionKey: false,
      transform(doc, ret) {
        delete ret._id;
      },
    },
    toJSON: {
      virtuals: true,
      versionKey: false,
      transform(doc, ret) {
        delete ret._id;
      },
    },
  },
);

HostSchema.virtual('followers').get(function fn(this: { social: SocialList }) {
  return (
    (this.social?.facebook?.followers || 0)
    + +(this.social?.instagram?.followers || 0)
    + +(this.social?.tiktok?.followers || 0)
  );
});

HostSchema.virtual(tagsConstants.ref.tags, {
  ref: tagsConstants.models.tags,
  localField: 'tags._id',
  foreignField: '_id',
  justOne: true,
});
