/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 12/08/2021
 */

import { ApiPropertyOptional } from '@nestjs/swagger';
import { SocialItem } from './social-item';

export class SocialList {
  @ApiPropertyOptional({ type: SocialItem })
  facebook?: SocialItem;

  @ApiPropertyOptional({ type: SocialItem })
  instagram?: SocialItem;

  @ApiPropertyOptional({ type: SocialItem })
  tiktok?: SocialItem;
}
