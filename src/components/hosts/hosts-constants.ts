/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 12/08/2021
 */

const hostsConstants = {
  models: {
    hosts: 'hosts',
  },
};

export default hostsConstants;
