/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 12/08/2021
 */

import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { JwtModule } from '@nestjs/jwt';
import authConstants from '@components/auth/auth-constants';
import UsersModule from '@components/users/users.module';
import usersConstants from '@components/users/users-constants';
import { UserSchema } from '@components/users/schemas/users.schema';
import tagsConstants from '@components/tags/tags-constants';
import { TagSchema } from '@components/tags/schemas/tags.schema';
import { TagsModule } from '@components/tags/tags.module';
import UsersRepository from '@components/users/users.repository';
import HostsRepository from './hosts.repository';
import { HostSchema } from './schemas/hosts.schema';
import hostsConstants from './hosts-constants';
import { HostsController } from './hosts.controller';
import { HostsService } from './hosts.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: hostsConstants.models.hosts,
        schema: HostSchema,
      },
      {
        name: usersConstants.models.users,
        schema: UserSchema,
      },
      {
        name: tagsConstants.models.tags,
        schema: TagSchema,
      },
    ]),
    JwtModule.register({
      secret: authConstants.jwt.secret,
    }),
    UsersModule,
    TagsModule,
  ],
  providers: [HostsService, HostsRepository, UsersRepository],
  controllers: [HostsController],
  exports: [HostsService, HostsRepository],
})
export class HostsModule { }
