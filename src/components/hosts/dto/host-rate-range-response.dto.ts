/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 28/08/2021
 */

import { ApiPropertyOptional } from '@nestjs/swagger';

export default class HostRateRangeResponseDto {
  @ApiPropertyOptional() max?: number;

  @ApiPropertyOptional() min?: number;
}
