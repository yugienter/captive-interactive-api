/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 12/08/2021
 */

import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export default class HostResDto {
  @ApiProperty() data: any;

  @ApiPropertyOptional() total?: number;

  @ApiPropertyOptional() page?: number;

  @ApiPropertyOptional() perPage?: number;

  @ApiPropertyOptional() lastPage?: number;
}
