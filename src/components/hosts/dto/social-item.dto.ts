/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 12/08/2021
 */

import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class SocialItemDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  link?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  followers?: number;
}
