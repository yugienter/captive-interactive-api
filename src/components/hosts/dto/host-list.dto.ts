/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 12/08/2021
 */

import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import HostDto from './host.dto';

export default class HostListDto {
  @ApiProperty({ type: [HostDto] })
  @IsNotEmpty()
  readonly hosts: HostDto[] = [];
}
