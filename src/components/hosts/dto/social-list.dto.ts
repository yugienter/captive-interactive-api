/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 12/08/2021
 */

import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsObject, IsOptional } from 'class-validator';
import { SocialItemDto } from './social-item.dto';

export class SocialListDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsObject()
  facebook?: SocialItemDto;

  @ApiPropertyOptional()
  @IsOptional()
  @IsObject()
  instagram?: SocialItemDto;

  @ApiPropertyOptional()
  @IsOptional()
  @IsObject()
  tiktok?: SocialItemDto;
}
