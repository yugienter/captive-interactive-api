/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 12/08/2021
 */

import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsArray,
  IsNumber,
  IsObject,
  IsOptional,
  IsString,
} from 'class-validator';
import { SocialListDto } from './social-list.dto';

export default class HostDto {
  @ApiProperty()
  // @IsNotEmpty()
  @IsOptional()
  @IsString()
  readonly email!: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  readonly name?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  readonly title?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  readonly age?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  readonly sex?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  readonly weight?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  readonly height?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsObject()
  readonly social?: SocialListDto;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  readonly phoneNumber?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsArray()
  readonly photo?: string[];

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  readonly rates?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  readonly experience?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  readonly aboutMe?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  readonly porfolio?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsArray()
  readonly tags?: string[];
}
