/**
* Author : phu.nguyenluu@gmail.com
* Setup : 11/08/2021
*/

import { ObjectID } from 'mongodb';
import { Model, UpdateWriteOpResult } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Injectable } from '@nestjs/common';

import SignUpDto from '@components/auth/dto/sign-up.dto';
import { BaseRepository } from '@shared/base.repository';
import { UserEntity } from './schemas/users.schema';

import UpdateUserDto from './dto/update-user.dto';

import usersConstants from './users-constants';

@Injectable()
export default class UsersRepository extends BaseRepository<UserEntity> {
  constructor(
    @InjectModel(usersConstants.models.users) private usersModel: Model<UserEntity>,
  ) {
    super(usersModel);
  }

  /**
   * Author : phu.nguyenluu@gmail.com
   * Updated : 25/08/2021
   */
  public createNewUser(user: SignUpDto): Promise<UserEntity> {
    return this.usersModel.create({
      ...user,
      verified: false,
    });
  }

  public updatePasswordByEmail(user: SignUpDto, password: string): Promise<UserEntity | UpdateWriteOpResult> {
    return this.usersModel.updateOne(
      {
        email: user.email,
      },
      {
        $set: { password },
      },
      { new: true },
    ).exec();
  }

  public getByEmail(email: string, verified: boolean = true): Promise<UserEntity | null> {
    return this.usersModel.findOne({
      email,
      verified,
    }).exec();
  }

  public getById(id: ObjectID, verified: boolean = true): Promise<UserEntity | null> {
    return this.usersModel.findOne({
      _id: id,
      verified,
    }, { password: 0 }).exec();
  }

  public updateVerify(id: ObjectID, data: UpdateUserDto): Promise<UserEntity | UpdateWriteOpResult> {
    return this.usersModel.updateOne(
      {
        _id: id,
      },
      {
        $set: data,
      },
    ).exec();
  }

  public getAll(verified: boolean): Promise<UserEntity[] | []> {
    return this.usersModel.find({
      verified,
    }, { password: 0 }).exec();
  }
}
