/**
* Author : phu.nguyenluu@gmail.com
* Setup : 11/08/2021
*/

import * as bcrypt from 'bcrypt';

import { ObjectID } from 'mongodb';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import SignUpDto from '@components/auth/dto/sign-up.dto';
import { UpdateWriteOpResult } from 'mongoose';
import CreatePasswordDto from '@components/auth/dto/create-password.dto';
import { UserEntity } from './schemas/users.schema';

import UsersRepository from './users.repository';
import UpdateUserDto from './dto/update-user.dto';
import CreateNewUserDto from './dto/create-new-user.dto';

@Injectable()
export default class UsersService {
  constructor(private readonly usersRepository: UsersRepository) { }

  private async _createPassword(password: string): Promise<string> {
    return bcrypt.hash(password, 10);
  }

  public async create(user: SignUpDto): Promise<UserEntity> {
    const hashedPassword = await this._createPassword(user.password);

    return this.usersRepository.createNewUser({
      password: hashedPassword,
      email: user.email,
      role: user.role,
    });
  }

  public async createNewUser(newUser: CreateNewUserDto): Promise<UserEntity> {
    const { email, role } = newUser;
    const exist = await this.usersRepository.findOne({
      email, role,
    });
    if (exist) {
      throw new HttpException('Invalid Params', HttpStatus.BAD_REQUEST);
    }
    return this.usersRepository.create(newUser);
  }

  public async createPassword(user: CreatePasswordDto): Promise<boolean> {
    const { email, role, password } = user;
    const verifyUser = await this.usersRepository.findOne({
      email, role, verified: true,
    });
    if (!verifyUser) {
      throw new HttpException('Invalid Params', HttpStatus.BAD_REQUEST);
    }
    const hashedPassword = await this._createPassword(password);
    const createdPassword = await this.usersRepository.updatePasswordByEmail(user, hashedPassword);
    return !!createdPassword;
  }

  public getByEmail(email: string, verified = true): Promise<UserEntity | null> {
    return this.usersRepository.getByEmail(email, verified);
  }

  public getById(id: ObjectID, verified = true): Promise<UserEntity | null> {
    return this.usersRepository.getById(id, verified);
  }

  public update(id: ObjectID, data: UpdateUserDto): Promise<UserEntity | UpdateWriteOpResult> {
    return this.usersRepository.updateVerify(id, data);
  }

  public getAll(verified: boolean = true): Promise<UserEntity[] | []> {
    return this.usersRepository.getAll(verified);
  }
}
