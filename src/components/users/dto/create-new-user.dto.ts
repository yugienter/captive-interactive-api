/**
* Author : phu.nguyenluu@gmail.com
* Setup : 11/08/2021
*/

import { RolesEnum } from '@decorators/roles.decorator';
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export default class CreateNewUserDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsEmail()
  readonly email: string = 'yopmail@yopmail.com';

  @ApiProperty({ enum: RolesEnum, default: RolesEnum.brand })
  @IsNotEmpty()
  @IsString()
  readonly role: RolesEnum = RolesEnum.brand;
}
