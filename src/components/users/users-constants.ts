/**
* Author : phu.nguyenluu@gmail.com
* Setup : 11/08/2021
*/

const usersConstants = {
  models: {
    users: 'users',
  },
};

export default usersConstants;
