/**
* Author : phu.nguyenluu@gmail.com
* Setup : 10/08/2021
*/

import AppService from '@components/app/app.service';
import { ApiOkResponse } from '@nestjs/swagger';
import { Controller, Get } from '@nestjs/common';

@Controller()
export default class AppController {
  constructor(private readonly appService: AppService) { }

  @ApiOkResponse({ description: 'Returns you api service of Live Commerce!' })
  @Get()
  sayHello(): string {
    return this.appService.getHello();
  }
}
