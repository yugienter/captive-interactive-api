/**
* Author : phu.nguyenluu@gmail.com
* Setup : 10/08/2021
*/

// registers aliases, DON'T REMOVE THIS LINE!
import 'module-alias/register';

import { NestFactory } from '@nestjs/core';
import { ValidationPipe, ValidationError } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import mongoodb from 'mongoose';
import ValidationExceptions from './exceptions/validation.exceptions';

import AppModule from './components/app/app.module';

import AllExceptionsFilter from './filters/all-exceptions.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalPipes(new ValidationPipe({
    exceptionFactory: (errors: ValidationError[]) => new ValidationExceptions(errors),
  }));
  app.useGlobalFilters(new AllExceptionsFilter());

  const port = process.env.PORT || 3000;

  const options = new DocumentBuilder()
    .setTitle('Api Live Commerce v1')
    .setDescription('The API of Live Commerce for devs')
    .setVersion('1.0')
    .addBearerAuth({ in: 'header', type: 'http' })
    .build();
  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup('api', app, document);

  if (process.env.SERVER_HOST?.indexOf(`localhost:${port}`)) {
    mongoodb.set('debug', true);
  }

  app.enableCors();

  await app.listen(port, async () => {
    console.log(`The server is running on ${port} port: ${process.env.SERVER_HOST}/api`);
  });
}
bootstrap();
