# Captive Interactive Server

This is the repo to make API of Captive App.

## Setup.

Clone this source.

Go to the source folder

_Install_

```
npm install
```

<br/>

_Build_

```
npm run build
```

<br/>

_Start_

Fisrt to edit the .env to link Database and Mailserver and Server host and Redis.

Then start

```
npm run start:dev
```

This to start the app with status developer
